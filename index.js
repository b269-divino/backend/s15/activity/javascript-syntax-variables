// console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


	let firstName = 'Juan';
	console.log("First Name: " + firstName);

	let lastName = 'Dela Cruz';
	console.log("Last Name: " + lastName);


	let ageNumeric = 30;
	console.log("Age: " + ageNumeric);

	let hobbies = 'Hobbies'
	let activities = ['Biking', 'Mountain Climbing', 'Swimming'];
	console.log(hobbies + ': ');
	console.log(activities);

	let address = 'Work Address'
	let fullAddress = {
		houseNumber: '32',
		street: 'Washington',
		city: 'Lincoln',
		state: 'Nebraska'
	}
	console.log(address);
	console.log(fullAddress);






	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ['Tony', 'Bruce', 'Thor', 'Natasha', "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = 'My Full Profile: '
	let fullProfile = {
		username: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false

	}
	console.log(profile);
	console.log(fullProfile)
	

	let bestName = "Bucky Barnes";
	console.log("My bestfriend is: " + bestName);

	const lastLocation = "Arctic Ocean";
	 	  lastLocation1 = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);


